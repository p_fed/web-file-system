package com.zerozero.repository;

import com.zerozero.domain.DBFolder;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Frost on 15.07.2017.
 */
public interface FolderRepository extends CrudRepository<DBFolder, Long> {
    Iterable<DBFolder> findByParentFolderIsNull();

    Iterable<DBFolder> findByParentFolder(DBFolder folder);
}

package com.zerozero.repository;

import com.zerozero.domain.DBFile;
import com.zerozero.domain.DBFolder;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Frost on 18.07.2017.
 */
public interface FileRepository extends CrudRepository<DBFile, Long> {
    Iterable<DBFile> findByParentFolderIsNull();

    Iterable<DBFile> findByParentFolder(DBFolder folder);
}

package com.zerozero.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by Frost on 18.07.2017.
 */
@ConfigurationProperties("storage")
public class StorageProperties {
    private String location = "upload-dir";

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}

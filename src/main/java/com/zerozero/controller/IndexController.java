package com.zerozero.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Frost on 15.07.2017.
 */
@Controller
public class IndexController {
    @RequestMapping("/")
    String index(){
        return "redirect:/folder";
    }
}

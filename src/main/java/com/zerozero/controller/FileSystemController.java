package com.zerozero.controller;

import com.zerozero.domain.DBFile;
import com.zerozero.domain.DBFolder;
import com.zerozero.exception.StorageFileNotFoundException;
import com.zerozero.services.FileService;
import com.zerozero.services.FolderService;
import com.zerozero.services.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

/**
 * Created by Frost on 15.07.2017.
 */
@Controller
public class FileSystemController {
    @Autowired
    private FolderService folderService;

    @Autowired
    private FileService fileService;

    @Autowired
    private StorageService storageService;

    @RequestMapping(value = "/folder", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("folders", folderService.listRootFolders());
        model.addAttribute("files", fileService.listRootFiles());
        return "folder";
    }

    @RequestMapping("folder/{id}")
    public String get(@PathVariable Long id, Model model) {
        DBFolder folder = folderService.getFolderById(id);
        model.addAttribute("currentFolder", folder);
        model.addAttribute("parentFolder", folder.getParentFolder());
        model.addAttribute("folders", folderService.getChildFolders(folder));
        model.addAttribute("files", folder.getFiles());
        return "folder";
    }

    @RequestMapping("folder/edit/{id}")
    public String edit(@PathVariable Long id, Model model) {
        model.addAttribute("folder", folderService.getFolderById(id));
        return "folderForm";
    }

    @RequestMapping("folder/create")
    public String create(Model model) {
        model.addAttribute("folder", new DBFolder());
        return "folderForm";
    }

    @RequestMapping("folder/{id}/create")
    public String create(@PathVariable Long id, Model model) {
        DBFolder parentFolder = folderService.getFolderById(id);
        model.addAttribute("folder", new DBFolder(parentFolder));
        return "folderForm";
    }

    @RequestMapping(value = "folder", method = RequestMethod.POST)
    public String save(DBFolder folder) {
        folderService.saveFolder(folder);
        DBFolder parentFolder = folder.getParentFolder();
        if (parentFolder != null)
            return "redirect:/folder/" + parentFolder.getId();
        else
            return "redirect:/folder";
    }

    @RequestMapping("folder/delete/{id}")
    public String delete(@PathVariable Long id) {
        folderService.deleteFolder(id);
        return "redirect:/folder";
    }

    @RequestMapping("folder/file/upload")
    public String upload(Model model) {
        model.addAttribute("dbFile", new DBFile());
        return "uploadFileForm";
    }

    @RequestMapping("folder/{id}/file/upload")
    public String upload(@PathVariable Long id, Model model) {
        DBFolder parentFolder = folderService.getFolderById(id);
        model.addAttribute("dbFile", new DBFile(parentFolder));
        return "uploadFileForm";
    }

    @Transactional
    @RequestMapping(value = "uploadFile", method = RequestMethod.POST)
    public String save(MultipartFile file, DBFile dbFile) {
        String originalFilename = file.getOriginalFilename();
        String storageFilename = UUID.randomUUID().toString();
        dbFile.setStorageName(storageFilename);
        dbFile.setName(originalFilename);
        dbFile.setSize(file.getSize());
        fileService.saveFile(dbFile);
        storageService.store(file, storageFilename);
        DBFolder parentFolder = dbFile.getParentFolder();
        if (parentFolder != null)
            return "redirect:/folder/" + parentFolder.getId();
        else
            return "redirect:/folder";
    }

    @GetMapping(value = "file/{id}")
    @ResponseBody
    public ResponseEntity<Resource> download(@PathVariable Long id, HttpServletResponse response) {
        DBFile dbFile = fileService.getFileById(id);
        String fileName = dbFile.getStorageName();
        Resource file = storageService.loadAsResource(fileName);
        return ResponseEntity
                .ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + dbFile.getName() + "\"")
                .body(file);
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

    @Transactional
    @RequestMapping("file/delete/{id}")
    public String deleteFile(@PathVariable Long id) {
        DBFile file = fileService.getFileById(id);
        storageService.delete(file.getName());
        fileService.deleteFile(id);
        return "redirect:/folder";
    }

    @RequestMapping("file/edit/{id}")
    public String editFile(@PathVariable Long id, Model model) {
        model.addAttribute("file", fileService.getFileById(id));
        return "fileForm";
    }

    @RequestMapping(value = "file", method = RequestMethod.POST)
    public String save(DBFile file) {
        fileService.saveFile(file);
        DBFolder parentFolder = file.getParentFolder();
        if (parentFolder != null)
            return "redirect:/folder/" + parentFolder.getId();
        else
            return "redirect:/folder";
    }
}

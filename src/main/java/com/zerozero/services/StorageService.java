package com.zerozero.services;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;

/**
 * Created by Frost on 18.07.2017.
 */
public interface StorageService {
    void init();

    void store(MultipartFile file, String storageFilename);

    Path load(String filename);

    Resource loadAsResource(String filename);

    void deleteAll();

    void delete(String fileName);
}

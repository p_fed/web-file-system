package com.zerozero.services;

import com.zerozero.domain.DBFolder;

/**
 * Created by Frost on 15.07.2017.
 */
public interface FolderService {
    Iterable<DBFolder> listRootFolders();

    Iterable<DBFolder> getChildFolders(DBFolder folder);

    DBFolder getFolderById(Long id);

    DBFolder saveFolder(DBFolder folder);

    void deleteFolder(Long id);
}

package com.zerozero.services;

import com.zerozero.domain.DBFile;
import com.zerozero.domain.DBFolder;
import com.zerozero.repository.FileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * Created by Frost on 18.07.2017.
 */
@Service
public class FileServiceImpl implements FileService {

    @Autowired
    FileRepository fileRepository;

    @Override
    public Iterable<DBFile> listRootFiles() {
        return fileRepository.findByParentFolderIsNull();
    }

    @Override
    public Iterable<DBFile> listFolderFiles(DBFolder folder) {
        return fileRepository.findByParentFolder(folder);
    }

    @Override
    public DBFile getFileById(Long id) {
        return fileRepository.findOne(id);
    }

    @Override
    public DBFile saveFile(DBFile file) {
        return fileRepository.save(file);
    }

    @Override
    public void deleteFile(Long id) {
        fileRepository.delete(id);
    }
}

package com.zerozero.services;

import com.zerozero.domain.DBFile;
import com.zerozero.domain.DBFolder;

/**
 * Created by Frost on 18.07.2017.
 */
public interface FileService {
    Iterable<DBFile> listRootFiles();

    Iterable<DBFile> listFolderFiles(DBFolder folder);

    DBFile getFileById(Long id);

    DBFile saveFile(DBFile file);

    void deleteFile(Long id);
}

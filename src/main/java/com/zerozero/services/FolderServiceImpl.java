package com.zerozero.services;

import com.zerozero.domain.DBFolder;
import com.zerozero.repository.FolderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Frost on 15.07.2017.
 */
@Service
public class FolderServiceImpl implements FolderService {
    @Autowired
    private FolderRepository folderRepository;

    @Override
    public Iterable<DBFolder> listRootFolders() {
        return folderRepository.findByParentFolderIsNull();
    }

    @Override
    public Iterable<DBFolder> getChildFolders(DBFolder folder) {
        return folderRepository.findByParentFolder(folder);
    }

    @Override
    public DBFolder getFolderById(Long id) {
        return folderRepository.findOne(id);
    }

    @Override
    public DBFolder saveFolder(DBFolder folder) {
        return folderRepository.save(folder);
    }

    @Override
    public void deleteFolder(Long id) {
        folderRepository.delete(id);
    }
}

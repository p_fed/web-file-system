package com.zerozero.domain;

import javax.persistence.*;

/**
 * Created by Frost on 15.07.2017.
 */
@Entity
@Table(name = "FILE")
public class DBFile {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @ManyToOne
    private DBFolder parentFolder;

    @Column(name = "NAME")
    private String name;

    @Column(name = "SIZE")
    private long size;

    @Column(name = "STORAGE_NAME")
    private String storageName;

    public DBFile() {
    }

    public DBFile(DBFolder parentFolder) {
        this.parentFolder = parentFolder;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DBFolder getParentFolder() {
        return parentFolder;
    }

    public void setParentFolder(DBFolder parentFolder) {
        this.parentFolder = parentFolder;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }
}

package com.zerozero.domain;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Frost on 15.07.2017.
 */
@Entity
@Table(name = "FOLDER")
public class DBFolder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private long id;

    @ManyToOne
    private DBFolder parentFolder;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "parentFolder", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<DBFolder> folders = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "parentFolder", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<DBFile> files = new HashSet<>();

    @Column(name = "NAME")
    private String name;

    public DBFolder() {
    }

    public DBFolder(String name) {
        this.name = name;
    }

    public DBFolder(DBFolder parentFolder) {
        this.parentFolder = parentFolder;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DBFolder getParentFolder() {
        return parentFolder;
    }

    public void setParentFolder(DBFolder parentFolder) {
        this.parentFolder = parentFolder;
    }

    public Set<DBFolder> getFolders() {
        return folders;
    }

    public void setFolders(Set<DBFolder> folders) {
        this.folders = folders;
    }

    public Set<DBFile> getFiles() {
        return files;
    }

    public void setFiles(Set<DBFile> files) {
        this.files = files;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
